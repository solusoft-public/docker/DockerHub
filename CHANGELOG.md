# CHANGELOG

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).
  
## [18-ubuntu22] - 2023-09-28  
  
### Changed  
  
* Ahora el número de versión hace referencia a la versión de Node instalada y a la versión del sistema operativo.  
* Actualizamos imagen a versión 18 de Node y a Ubuntu 22.04.  
  