# DockerHub Dockerfiles

Imagenes base subidas a DockerHub en la cuenta solusoft.

[Instrucciones en la Wiki](https://solusoft1.sharepoint.com/Desarrollo/Publicar%20ImagenBase%20en%20DockerHub.aspx)

[Repositorio en Dockerhub](https://hub.docker.com/r/solusoft/)